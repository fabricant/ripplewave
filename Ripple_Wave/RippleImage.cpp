﻿#define _USE_MATH_DEFINES

#include "RippleImage.h"
#include <math.h>
#include <time.h>
#include <process.h>

struct pixel {
	unsigned char b, g, r, a;

};


typedef struct
{
	int xCenter;
	int yCenter;
	int radius;
	int length;
	BOOL needRemove;
} Round;

using namespace std;
int length = 60;
unsigned int fps;
BOOL multiThread;
HWND childWindow;
HDC processedImageDC;
std::list <Round> roundList;
pixel *originalImagePixel, *processedImagePixel;
int *deltaArray;
int *mixingArray;
inline void movePoint(int x, int y, int deltaX);
size_t pixelCount;
HANDLE event;
void updateFrame();
BOOL needUpdate;
void updateRound(list<Round>::iterator pRound);
unsigned __stdcall updateRound(void *firstElement);

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;

	switch (message)
	{
	case WM_CREATE:
	{
		HDC hdcMem;
		childWindow = hWnd;
		deltaArray = new int[MAX_LENGTH];
		fps = 1000 / 30;
		multiThread = false;
		event = CreateEvent(NULL, true, true, NULL);
		srand(0);
		SetTimer(hWnd, 1, 3000, NULL);

		setAngleCoefficient(1.5f);

		//get device context
		HDC hdc = GetDC(hWnd);

		// create a context compatible to a context of the current device(hdc)
		hdcMem = CreateCompatibleDC(hdc);
		//Selects the images in a context of the device compatible to the current device 
		SelectObject(hdcMem, hbmp);
		//create DIB section and we receive pointers on its pixel
		getPixelPoimter(bm, hWnd, hdc);
		// copy the image in the section created Dib
		BitBlt(processedImageDC, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);
		pixelCount = bm.bmWidth * bm.bmHeight;
		mixingArray = new int[pixelCount];
		memset(mixingArray, 0, sizeof(int) * pixelCount);
		originalImagePixel = new pixel[pixelCount];
		memcpy(originalImagePixel, processedImagePixel, sizeof(pixel) * pixelCount);
		ReleaseDC(hWnd, hdc);
		DeleteDC(hdcMem);
		needUpdate = true;
		CreateThread(NULL, NULL, &tickThreadProc, NULL, NULL, NULL);

		//change the window size
		MoveWindow(childWindow, 0, 60, bm.bmWidth, bm.bmHeight, true);

		Round rnd;
		rnd.xCenter = bm.bmWidth / 2;
		rnd.yCenter = bm.bmHeight / 2;
		rnd.radius = 0;
		rnd.length = 1;
		rnd.needRemove = false;
		roundList.push_back(rnd);

	}
	break;
	case WM_PAINT:
	{
		updateFrame();
		HDC hdc = BeginPaint(hWnd, &ps);
		BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmHeight, processedImageDC, 0, 0, SRCCOPY);
		memcpy(processedImagePixel, originalImagePixel, sizeof(pixel) * bm.bmWidth * bm.bmHeight);

		EndPaint(hWnd, &ps);

	}
	break;
	case WM_LBUTTONDOWN:
	{
		int dx = 0;
		int dy = 0;
		dx = LOWORD(lParam);
		dy = HIWORD(lParam);
		Round *rnd = new Round;
		rnd->xCenter = dx;
		rnd->yCenter = dy;
		rnd->radius = 0;
		rnd->length = 1;
		rnd->needRemove = false;
		if (multiThread)
		{
			(HANDLE)_beginthreadex(NULL, 0, updateRound, rnd, 0, NULL);
		}
		else
		{
			roundList.push_back(*rnd);
			delete rnd;
		}
		//round 
	}
	break;
	case WM_TIMER:
	{
		Round *rnd = new Round;
		rnd->xCenter = rand() % bm.bmWidth;
		rnd->yCenter = rand() % bm.bmHeight;
		rnd->radius = 0;
		rnd->length = 1;
		rnd->needRemove = false;
		if (multiThread)
		{
			(HANDLE)_beginthreadex(NULL, 0, updateRound, rnd, 0, NULL);
		}
		else
		{
			roundList.push_back(*rnd);
			delete rnd;
		}
	}
		break;
	case WM_DESTROY:
		needUpdate = false;
		ResetEvent(event);
		WaitForSingleObject(event, INFINITE);
		PostQuitMessage(0);
		CloseHandle(event);
		delete[] deltaArray;
		delete[] originalImagePixel;
		delete[] mixingArray;
		break; 

	default: return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void getPixelPoimter(BITMAP bm, HWND hWnd, HDC hdc)
{
	BITMAPINFO bmi;
	bmi.bmiHeader.biSize = sizeof(BITMAPINFO);
	bmi.bmiHeader.biWidth = bm.bmWidth;
	bmi.bmiHeader.biHeight = -bm.bmHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32; 
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biSizeImage = 0;
	bmi.bmiHeader.biXPelsPerMeter = 0;
	bmi.bmiHeader.biYPelsPerMeter = 0;
	bmi.bmiHeader.biClrUsed = 0;
	bmi.bmiHeader.biClrImportant = 0;

	processedImageDC = CreateCompatibleDC(hdc);
	hbmp = CreateDIBSection(hdc, &bmi, DIB_RGB_COLORS, (void**)&processedImagePixel, NULL, 0);
	SelectObject(processedImageDC, hbmp);
}

DWORD WINAPI tickThreadProc(HANDLE handle) {
	while (needUpdate)
	{
		newFrame();
		SetEvent(event);
		Sleep(fps);

	}
	SetEvent(event);

	return 0;
}

void newFrame()
{
	InvalidateRect(childWindow, NULL, false);
	UpdateWindow(childWindow);

	list<Round>::iterator it = roundList.begin();

	while (it != roundList.end() && needUpdate)
		{
			updateRound(it);
			if (it->needRemove)
			{
				roundList.erase(it++);
			}
			else
			{
				++it;

			}
		}
}



void updateRound(list<Round>::iterator pRound)
{

	int bigRadius = pRound->radius + pRound->length;

	if (pRound->xCenter - bigRadius < 0 || pRound->xCenter + bigRadius > bm.bmWidth || pRound->yCenter - bigRadius < 0 || pRound->yCenter + bigRadius > bm.bmHeight || !needUpdate)
	{
		pRound->needRemove = true;
		return;
	}

	int sqBigRadius = bigRadius * bigRadius;
	int sqSmallRadius = pRound->radius * pRound->radius;
	for (int x = bigRadius; x > pRound->radius; --x)
	{
		int sqX = x*x;
		int y = sqBigRadius - (sqX);
		y = (int)sqrt(float(y));
		for (int i = 1; i < y; ++i)
		{
			int deltaX = deltaArray[int(sqrt(float(i*i + sqX)) - pRound->radius - 1)];
			int xMap = x + pRound->xCenter;
			int yMap = i + pRound->yCenter;
			movePoint(xMap, yMap, deltaX);

			xMap = pRound->xCenter - x;
			movePoint(xMap, yMap, deltaX);

			yMap = -i + pRound->yCenter;
			movePoint(xMap, yMap, deltaX);

			xMap = pRound->xCenter + x;
			movePoint(xMap, yMap, deltaX);
		}

		int deltaX = deltaArray[ int(x - pRound->radius - 1) ];
		int xMap = x + pRound->xCenter;
		int yMap = pRound->yCenter;
		movePoint(xMap, yMap, deltaX);

		xMap = pRound->xCenter - x;
		movePoint(xMap, yMap, deltaX);
	}

	for (int x = pRound->radius; x > 0; --x)
	{
		int sqX = x*x;
		int  yBig = sqBigRadius - (sqX);
		int ySmall = sqSmallRadius - (sqX);
		yBig = (int)sqrt(float(yBig));
		ySmall = (int)sqrt(float(ySmall));
		for (int i = ySmall; i < yBig; ++i)
		{
			int deltaX = deltaArray[(int)(sqrt(float(i*i + sqX)) - pRound->radius)];
			int xMap = x + pRound->xCenter;
			int yMap = i + pRound->yCenter;
			movePoint(xMap, yMap, deltaX);

			xMap = -x + pRound->xCenter;
			movePoint(xMap, yMap, deltaX);

			yMap = -i + pRound->yCenter;
			movePoint(xMap, yMap, deltaX);

			xMap = x + pRound->xCenter;
			movePoint(xMap, yMap, deltaX);

		}

	}


	for (int i = pRound->radius; i < bigRadius; ++i)
	{
		int deltaX = deltaArray[ int(i - pRound->radius)];
		int xMap = pRound->xCenter;
		int yMap = i + pRound->yCenter;
		movePoint(xMap, yMap, deltaX);

		yMap = -i + pRound->yCenter;

		movePoint(xMap, yMap, deltaX);
	}

	if (pRound->length < length)
	{
		++pRound->length;
	}

	++pRound->radius;

	return;
}


unsigned __stdcall updateRound(void *firstElement)
{
	Round *pRound = (Round*)firstElement;

	while(needUpdate)
	{
		int bigRadius = pRound->radius + pRound->length;

		if (pRound->xCenter - bigRadius < 0 || pRound->xCenter + bigRadius > bm.bmWidth || pRound->yCenter - bigRadius < 0 || pRound->yCenter + bigRadius > bm.bmHeight)
		{
			pRound->needRemove = true;
			delete pRound;
			return 0;
		}
		int sqBigRadius = bigRadius * bigRadius;
		int sqSmallRadius = pRound->radius * pRound->radius;
		for (int x = bigRadius; x > pRound->radius; --x)
		{
			int sqX = x*x;
			int y = sqBigRadius - (sqX);
			y = (int)sqrt(float(y));
			for (int i = 1; i < y; ++i)
			{
				int deltaX = deltaArray[int(sqrt(float(i*i + sqX)) - pRound->radius - 1)];
				int xMap = x + pRound->xCenter;
				int yMap = i + pRound->yCenter;
				movePoint(xMap, yMap, deltaX);

				xMap = pRound->xCenter - x;
				movePoint(xMap, yMap, deltaX);

				yMap = -i + pRound->yCenter;
				movePoint(xMap, yMap, deltaX);

				xMap = pRound->xCenter + x;
				movePoint(xMap, yMap, deltaX);
			}

			int deltaX = deltaArray[int(x - pRound->radius - 1)];
			int xMap = x + pRound->xCenter;
			int yMap = pRound->yCenter;
			movePoint(xMap, yMap, deltaX);

			xMap = pRound->xCenter - x;
			movePoint(xMap, yMap, deltaX);
		}

		for (int x = pRound->radius; x > 0; --x)
		{
			int sqX = x*x;
			int  yBig = sqBigRadius - (sqX);
			int ySmall = sqSmallRadius - (sqX);
			yBig = (int)sqrt(float(yBig));
			ySmall = (int)sqrt(float(ySmall));
			for (int i = ySmall; i < yBig; ++i)
			{
				int deltaX = deltaArray[(int)(sqrt(float(i*i + sqX)) - pRound->radius)];
				int xMap = x + pRound->xCenter;
				int yMap = i + pRound->yCenter;
				movePoint(xMap, yMap, deltaX);

				xMap = -x + pRound->xCenter;
				movePoint(xMap, yMap, deltaX);

				yMap = -i + pRound->yCenter;
				movePoint(xMap, yMap, deltaX);

				xMap = x + pRound->xCenter;
				movePoint(xMap, yMap, deltaX);

			}

		}
		 

		for (int i = pRound->radius; i < bigRadius; ++i)
		{
			int deltaX = deltaArray[int(i - pRound->radius)];
			int xMap = pRound->xCenter;
			int yMap = i + pRound->yCenter;
			movePoint(xMap, yMap, deltaX);

			yMap = -i + pRound->yCenter;

			movePoint(xMap, yMap, deltaX);
		}

		if (pRound->length < length)
		{
			++pRound->length;
		}

		++pRound->radius;
		ResetEvent(event);
		WaitForSingleObject(event, INFINITE);

	}
	//SetEvent(event);

	delete pRound;
	return 0;
}

inline void movePoint(int x, int y, int deltaX)
{
	size_t pixelNumber = y*bm.bmWidth + x;
	y += deltaX;
	x += deltaX;

	mixingArray[pixelNumber] += (y*bm.bmWidth + x) - pixelNumber;
}


void updateFrame()
{
	for (size_t i = 0; i < pixelCount; ++i)
	{
		if (mixingArray[i] == 0)
		{
			continue;
		}

		int newPixelNumber = mixingArray[i] + i;
		if (newPixelNumber < 0)
		{
			newPixelNumber = pixelCount + newPixelNumber;  
		}

		processedImagePixel[i] = originalImagePixel[newPixelNumber];

		mixingArray[i] = 0;
	}
}

void setSpeed(int x)
{
	fps = 1000 / x;
}

void setAngleCoefficient(float coefficient)
{
	for (int i = 0; i < length; ++i)
	{
		float angle = float(i) / length;
		angle = sin(angle * 9);
		angle /= coefficient;

		deltaArray[i] = int(length * cos(angle) - length);
	}
}

void setLength(int l)
{
	length = l;
	setAngleCoefficient(1.0);
}