﻿#pragma comment(lib, "comctl32.lib")

#include "stdio.h"
#include <process.h>
#include "RippleImage.h"
#include <tchar.h>
#include <CommCtrl.h>

HWND handleMainWindow;
LRESULT CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM);
void showOpenWindow(HINSTANCE hInst);
HBITMAP hbmp;
BITMAP bm;
HWND speedSlider;
HWND angleSlider;
HWND lengthSlider;
HWND multThreadCheckBox;
static TCHAR name[256] = _T("");

int APIENTRY _tWinMain(HINSTANCE This, HINSTANCE prev, LPTSTR cmd, int mode)
{
	
	MSG message;
	WNDCLASS window;
	window.hInstance = This;
	window.lpszClassName = _T("study");
	window.lpfnWndProc = MainWndProc;
	window.style = CS_HREDRAW | CS_VREDRAW;
	window.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	window.hCursor = LoadCursor(NULL, IDC_ARROW);
	window.lpszMenuName = NULL;
	window.cbClsExtra = 0;
	window.cbWndExtra = 0;
	window.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);

	if (!RegisterClass(&window))
		return 0;

	handleMainWindow = CreateWindow(_T("study"), _T("Study window"), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		HWND_DESKTOP,
		NULL,
		This,
		NULL);
	ShowWindow(handleMainWindow, mode); // Ïîêàçàòü îêíî
	//Зашрузка изображения из файла
	hbmp = (HBITMAP)LoadImage(NULL, _T("0003-Abstract.bmp"), IMAGE_BITMAP,
		0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	if (hbmp == NULL)
	{

		showOpenWindow(This);
		hbmp = (HBITMAP)LoadImage(NULL, name, IMAGE_BITMAP,
			0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
		if (hbmp == NULL)
		{
			MessageBox(handleMainWindow, _T("Could not open file"), _T("Error"),
				MB_OK | MB_ICONHAND);
			DestroyWindow(handleMainWindow);
			return 1;
		}
	}
	//  Получаем информацию о изображении и записуем ее в структуру bm
	GetObject(hbmp, sizeof(bm), &bm);

	MoveWindow(handleMainWindow, 0, 0, bm.bmWidth, bm.bmHeight + 60, true);

	WNDCLASS cWindow;
	cWindow.hInstance = This;
	cWindow.lpszClassName = _T("childWindow");
	cWindow.lpfnWndProc = WndProc;
	cWindow.style = CS_HREDRAW | CS_VREDRAW;
	cWindow.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	cWindow.hCursor = LoadCursor(NULL, IDC_ARROW);
	cWindow.lpszMenuName = NULL;
	cWindow.cbClsExtra = 0;
	cWindow.cbWndExtra = 0;
	cWindow.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);

	if (!RegisterClass(&cWindow))
		return 0;

	CreateWindow(_T("childWindow"), _T("Study window"), WS_CHILD | WS_BORDER | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, handleMainWindow,
		NULL,
		This,
		NULL);


	INITCOMMONCONTROLSEX sl;
	sl.dwSize = sizeof(INITCOMMONCONTROLSEX);
	sl.dwICC = ICC_BAR_CLASSES;
	InitCommonControlsEx(&sl);

	int widght = bm.bmWidth / 3;
	speedSlider = CreateWindowEx(NULL, TRACKBAR_CLASS, _T("slider"), WS_CHILD | WS_VISIBLE, 0, 0, widght, 30, handleMainWindow, NULL, This, NULL);
	DWORD error = GetLastError();
	SendMessage(speedSlider, TBM_SETRANGEMIN, (WPARAM)TRUE, 30);
	SendMessage(speedSlider, TBM_SETRANGEMAX, (WPARAM)TRUE, 200);

	angleSlider = CreateWindowEx(NULL, TRACKBAR_CLASS, _T("angleSlider"), WS_CHILD | WS_VISIBLE, widght, 0, widght, 30, handleMainWindow, NULL, This, NULL);
	error = GetLastError();
	SendMessage(angleSlider, TBM_SETRANGEMIN, (WPARAM)TRUE, 10);
	SendMessage(angleSlider, TBM_SETRANGEMAX, (WPARAM)TRUE, 30);
	SendMessage(angleSlider, TBM_SETPOS, (WPARAM)TRUE, 15);

	lengthSlider = CreateWindowEx(NULL, TRACKBAR_CLASS, _T("lenthSlider"), WS_CHILD | WS_VISIBLE, widght * 2, 0, widght, 30, handleMainWindow, NULL, This, NULL);
	error = GetLastError();
	SendMessage(lengthSlider, TBM_SETRANGEMIN, (WPARAM)TRUE, 30);
	SendMessage(lengthSlider, TBM_SETRANGEMAX, (WPARAM)TRUE, MAX_LENGTH);
	
	multThreadCheckBox = CreateWindowEx(NULL, _T("BUTTON"), _T("Multithread"), WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 0, 30, 100, 30, handleMainWindow, NULL, This, NULL);
	// Цикл обработки сообщений
	while (GetMessage(&message, NULL, 0, 0))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}

	return 0;
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		break;
	case WM_HSCROLL:
	{
		HWND slider = (HWND)lParam;
		LRESULT pos = SendMessage(slider, TBM_GETPOS, NULL, NULL);
		if (slider == speedSlider){
			setSpeed(pos);
			break;
		}

		if (slider == angleSlider)
		{
			setAngleCoefficient((float)pos / 10);
		}

		if (slider == lengthSlider)
		{
			setLength(pos);
		}
		break;
	}
	case WM_COMMAND:
	{
		LRESULT res = SendMessage(multThreadCheckBox, BM_GETCHECK, 0, 0);

		if (res == BST_CHECKED)
		{
			multiThread = true;
		}
		else
		{
			multiThread = false;
		}
	}
		break;

	case WM_DESTROY:
		break;
	default: return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

void showOpenWindow(HINSTANCE hInst)
{
	static OPENFILENAME file;
	file.lStructSize = sizeof(OPENFILENAME);
	file.hInstance = hInst;
	file.lpstrFilter = _T("Bmp 24bit (*.bmp)\0*.bmp\0");
	file.lpstrFile = name;
	file.nMaxFile = 256;
	file.lpstrInitialDir = _T(".\\");
	file.lpstrDefExt = _T("txt");
	file.Flags = OFN_HIDEREADONLY;
	file.lpstrTitle = _T("Открыть файл для чтения");
	file.Flags = OFN_HIDEREADONLY;
	GetOpenFileName(&file);
}