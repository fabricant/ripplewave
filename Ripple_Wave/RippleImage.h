﻿#ifndef RIPPLE_IMAGE
#define RIPPLE_IMAGE

#include <Windows.h>
#include <list>
#include <tchar.h>

#define MAX_LENGTH 120

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI tickThreadProc(HANDLE handle);

void getPixelPoimter(BITMAP bm, HWND hWnd, HDC hdc);
void newFrame();
void setSpeed(int x);
void setAngleCoefficient(float coefficient);
void setLength(int l);


//HDC memBit;
 extern HBITMAP hbmp;
 extern BITMAP bm;
 extern  HWND childWindow;
 extern int length;
 extern BOOL multiThread;
#endif // !RIPPLE_IMAGE